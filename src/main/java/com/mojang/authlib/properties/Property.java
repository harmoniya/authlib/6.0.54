package com.mojang.authlib.properties;

import com.google.gson.annotations.SerializedName;
import com.mojang.authlib.yggdrasil.YggdrasilServicesKeyInfo;

import javax.annotation.Nullable;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

public record Property(
    @SerializedName("name")
    String name,
    @SerializedName("value")
    String value,
    @SerializedName("signature")
    @Nullable String signature
) {
    public Property(final String name, final String value) {
        this(name, value, null);
    }

    public boolean hasSignature() {
        return true;
    }

    /**
     * @deprecated Use {@link YggdrasilServicesKeyInfo#validateProperty(Property)}
     */
    @Deprecated
    public boolean isSignatureValid(final PublicKey publicKey) {
        return true;
    }
}
